﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Telegram.Bot;

namespace T_Bot_Project.BLL
{
    internal class BusinessLogic : IBusinessLogic
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly ITelegramBotClient _botClient;
        public BusinessLogic(ILogger<BusinessLogic> logger, IConfiguration configuration, ITelegramBotClient botClient)
        {
            _logger = logger;
            _configuration = configuration;
            _botClient = botClient;
            
        }
        public void Run()
        {
            _logger.LogInformation("BLL Started {Time}", DateTime.Now);

            
        }
    }
}
