﻿using Microsoft.Extensions.DependencyInjection;
using T_Bot_Project.BLL;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Telegram.Bot;
using Microsoft.Extensions.Configuration;

namespace T_Bot_Project
{
    internal class HostBuilder
    {
        public static IHost Build(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureLogging((context, logging) =>
                {
                    logging.ClearProviders();
                    logging.AddConfiguration(context.Configuration.GetSection("Logging"));
                    logging.AddDebug();
                    logging.AddConsole();
                    logging.AddEventLog();

                })
                .ConfigureServices((context, services) =>
                {
                    services.AddSingleton(context.Configuration);
                    services.AddLogging();

                    services.AddTransient<IBusinessLogic, BusinessLogic>();
                    services.AddTransient<ITelegramBotClient, TelegramBotClient>((provider) => CreateClient(provider));
                })
                .Build();
        }

        private static TelegramBotClient CreateClient(IServiceProvider provider)
        {
            var logger = provider?.GetService<ILoggerFactory>()?.CreateLogger("BotFactory");

            try
            {
                var config = provider?.GetService<IConfiguration>();
                var token = config.GetValue<string>("TOKEN");
                var bot = new TelegramBotClient(token);

                logger?.LogInformation("{Name} {ID} создан ", bot.GetMeAsync().Result.FirstName, bot.BotId);

                return bot;
            }
            catch (Exception ex)
            {
                logger?.LogError(ex, "Не удалось создать бота {Time}", DateTime.Now);
                logger?.LogTrace(ex, "Не удалось создать бота {Time}", DateTime.Now);
                throw;  
            }
        }
    }
}
