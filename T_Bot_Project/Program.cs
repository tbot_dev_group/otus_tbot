﻿using Microsoft.Extensions.Hosting;
using T_Bot_Project.BLL;

using IHost host = T_Bot_Project.HostBuilder.Build(args);

var businessLogic = host.Services.
    GetService(typeof(IBusinessLogic)) as IBusinessLogic;

businessLogic?.Run();

await host.RunAsync();


